<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Aide</name>
    <message>
        <location filename="../Ui_help.py" line="152"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="151"/>
        <source>Mode d&apos;emploi</source>
        <translation>Usage</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="154"/>
        <source>ScolaSync, pour gerer les fichiers des baladeurs</source>
        <translation>Scolasync, to manage files in pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="155"/>
        <source>Numero de version :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="156"/>
        <source>Auteurs</source>
        <translation>Authors</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="157"/>
        <source>Licence</source>
        <translation>License</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="158"/>
        <source>Langues et traductions</source>
        <translation>Languages and translations</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="159"/>
        <source>A propos</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="160"/>
        <source>Fermer</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="153"/>
        <source>Manuel</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="154"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;Le manuel de Scolasync peut être consulté de plusieurs façons&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:8px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel très court, intégré au paquet :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel plus riche, à installer à part :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici&lt;/span&gt;&lt;/a&gt; (le choix du manuel peut être changé à l'aide des &lt;span style=&quot; font-style:italic;&quot;&gt;préférences&lt;/span&gt;).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel en ligne :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.ofset.org/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:18px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:xx-large; font-weight:600;&quot;&gt;Solasync's manual can be accessed in a few ways:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:8px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;From a short user manual, integrated with the package:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;cliquez ici&lt;/span&gt;&lt;/a&gt;.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Dans un manuel plus riche, à installer à part :&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;file:///usr/share/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;click here&lt;/span&gt;&lt;/a&gt; (the manual's choice can be changed with the &lt;span style=&quot; font-style:italic;&quot;&gt;preference dialog&lt;/span&gt;).&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;From an inline manual:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;http://www.ofset.org/scolasync/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;click here.&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="171"/>
        <source>Choix des fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="172"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="173"/>
        <source>Ordinateur</source>
        <translation>Desktop/Laptop</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="175"/>
        <source>Disque dur</source>
        <translation>Hard disk</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="195"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="177"/>
        <source>Répertoire de destination des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="179"/>
        <source>Répertoire de destination sur les baladeurs,&lt;br&gt;peut être modifié dans les préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="181"/>
        <source>Baladeur</source>
        <translation>Puggable device</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="184"/>
        <source>Disque flash</source>
        <translation>Flash disk</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="187"/>
        <source>Cle USB</source>
        <translation>USB stick</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="189"/>
        <source>Taille totale :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="191"/>
        <source>Ajouter a la liste</source>
        <translation>Add to the list</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="194"/>
        <source>Retirer de la liste</source>
        <translation>Remove from the list</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="196"/>
        <source>Abandonner</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="197"/>
        <source>Continuer ...</source>
        <translation>Continue...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>Double-clic non pris en compte</source>
        <translation>Double click is ignored</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="280"/>
        <source>pas d&apos;action pour l&apos;attribut %1</source>
        <translation type="obsolete">No action for attribute &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="503"/>
        <source>Choix de fichiers à supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="504"/>
        <source>Choix de fichiers à supprimer (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Aucun fichier sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Veuillez choisir au moins un fichier</source>
        <translation>Please choose at least one file</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="561"/>
        <source>Choix de fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="562"/>
        <source>Choix de fichiers à copier depuis les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="563"/>
        <source>Choix de la destination ...</source>
        <translation>Choose destination...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="575"/>
        <source>Choisir un répertoire de destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voir les copies</source>
        <translation>See the copies</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voulez-vous voir les fichiers copiés ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Destination manquante</source>
        <translation>Missing destination</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Veuillez choisir une destination pour la copie des fichiers</source>
        <translation>Please choose a destination to copy the files</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7273966"/>
        <source>Liste de périphériques modifiée pour Scolasync</source>
        <translation type="obsolete">Device list modified for Scolasyn</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="7273966"/>
        <source>Une clé USB ou un baladeur numérique au moins a change de statut. Vérifiez éventuellement la liste des périphériques de Scolasync</source>
        <translation type="obsolete">At least one USB stick or music player has chaged its status; eventually check the list of devices of Scolasync</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="140"/>
        <source>Zone de recherche</source>
        <translation>Search zone</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="141"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Répertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="147"/>
        <source>Le répertoire des clés&lt;br&gt;où se trouvent les&lt;br&gt;documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="148"/>
        <source>Le répertoire des clés où se trouvent les documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="149"/>
        <source>Choix de clé modèle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="151"/>
        <source>Choisir une des clés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="152"/>
        <source>Liste de fichiers à traiter (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="154"/>
        <source>Supprimer l'item sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="0"/>
        <source>Écrire le nom d'un fichier ou d'un répertoire.&lt;br&gt;Jokers autorisés</source>
        <translation type="obsolete">Write the name of a file or a directory&lt;br&gt;wildcards are OK</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="150"/>
        <source>Ajouter un fichier ou un filtre</source>
        <translation type="obsolete">Add a filename or a filter</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="162"/>
        <source>Rechercher (fichier) ...</source>
        <translation>Search (file)...</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="165"/>
        <source>Rechercher (répertoire) ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="92"/>
        <source>Preferences de ScolaSync</source>
        <translation>Preferences of Scolasync</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="93"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="7274524"/>
        <source>Les cases à cocher permettent de n'agir que sur une part des baladeurs</source>
        <translation type="obsolete">Checkboxes allow to deal with a subset of the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="7274524"/>
        <source>Utiliser des cases à cocher pour désigner les baladeurs</source>
        <translation type="obsolete">Use checkboxes to select pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="98"/>
        <source>Répertoire pour les travaux :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="99"/>
        <source>Travail</source>
        <translation>Homework</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="162"/>
        <source>%s kilo-octets</source>
        <translation>%s kBytes</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="165"/>
        <source>%s méga-octets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="168"/>
        <source>%s giga-octets</source>
        <translation>%s GBytes</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="213"/>
        <source>inconnu</source>
        <translation>**unknown**</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="212"/>
        <source>La cle %1&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation type="obsolete">The medium %1&lt;br&gt;is unknown, please give the owner's name</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="251"/>
        <source>Entrer un nom</source>
        <translation>Enter a name</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="511"/>
        <source>Vous allez effacer plusieurs baladeurs</source>
        <translation>You will erase many drives</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="100"/>
        <source>Fichier spécial pour le manuel :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="101"/>
        <source>/usr/share/scolasync/help/manualPage_fr_FR.html</source>
        <translation>/usr/share/scolasync/help/manualPage_en_US.html</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="127"/>
        <source>Recomptage automatique des médias :</source>
        <translation type="obsolete">Automatic counting of the medias:</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="128"/>
        <source>toutes les</source>
        <translation type="obsolete">every</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="129"/>
        <source>30 secondes</source>
        <translation type="obsolete">30 seconds</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="53"/>
        <source>%1 secondes</source>
        <translation type="obsolete">%1 seconds</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="160"/>
        <source>Ligne d'édition du nom d'un fichier ou d'un répertoire.&lt;br&gt;Jokers autorisés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="157"/>
        <source>Ajouter un fichier ou un filtre défini dans la ligne d'édition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="637"/>
        <source>Réitérer la dernière commande</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="547"/>
        <source>La dernière commande était&lt;br&gt;%1&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</source>
        <translation type="obsolete">The last command was&lt;br&gt;%1&lt;br&gt;Do you want to redo it with the new media?</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="96"/>
        <source>Efface les fichiers et les répertoires après copie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="94"/>
        <source>Si la case est cochée,&lt;br&gt;les données sont transférées sur le disque dur&lt;br&gt;puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="95"/>
        <source>Si la case est cochée, les données sont transférées sur le disque dur puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="114"/>
        <source>Choix d'un groupe d'élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="72"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="118"/>
        <source>Replier toutes les classes</source>
        <translation>Fold down checkboxes</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="119"/>
        <source>Tout replier</source>
        <translation>Fold down everything</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="120"/>
        <source>Cocher tous les élèves visibles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="121"/>
        <source>Cocher</source>
        <translation>Check the box</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="122"/>
        <source>Décocher tous les élèves, visibles ou non</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="123"/>
        <source>Décocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="125"/>
        <source>Ajout à la liste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="127"/>
        <source>Suppr. de la liste</source>
        <translation>Del from the list</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="132"/>
        <source>Valider</source>
        <translation>Validate</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="129"/>
        <source>N° à partir de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="128"/>
        <source>Quand la case est cochée, un numéro sera ajouté comme préfixe aux noms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="130"/>
        <source>La numérotation commence à cette valeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="124"/>
        <source>Ajouter les noms cochés de l'arbre dans la liste à droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="126"/>
        <source>Supprimer de la liste les noms en surbrillance</source>
        <translation>Delete the list of highlighted names</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="131"/>
        <source>Accepter la liste actuelle et fermer ce dialogue</source>
        <translation>Accept the current list and close this dialog</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="133"/>
        <source>Supprimer tous les noms de la liste et fermer ce dialogue</source>
        <translation>Delete all the names from the list and close this dialog</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="134"/>
        <source>Remettre à zéro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="69"/>
        <source>Choix du propriétaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Nouveau nom du propriétaire du baladeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="115"/>
        <source>Liste d'élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="116"/>
        <source>Ouvrir un fichier ...</source>
        <translation>Open a file...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="97"/>
        <source>Fichier des élèves :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="70"/>
        <source>Nommer le baladeur nouvellement connecté</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="71"/>
        <source>Changez le choix parmi cette liste si une autre ligne convient mieux</source>
        <translation>Choose another line in the list if it fits better</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="73"/>
        <source>nom actuel du baladeur</source>
        <translation>current name of the drive</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="74"/>
        <source>C'est le nom connu par votre ordinateur, s'il a déjà été défini dans le passé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="75"/>
        <source>nom proposé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="76"/>
        <source>Le nouveau nom proposé peut venir de la liste à gauche ou être modifié à la main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="77"/>
        <source>Renommer le baladeur</source>
        <translation>Rename the drive</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="78"/>
        <source>Choisir comme nouveau nom</source>
        <translation>Choose as a new name</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="79"/>
        <source>Fermer le dialogue sans rien faire</source>
        <translation>Close the dialog without doing anything</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="80"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../choixEleves.py" line="93"/>
        <source>Le fichier {schoolfile} n&apos;a pas pu &#xc3;&#xaa;tre trait&#xc3;&#xa9; : {erreur}</source>
        <translation type="unfinished">The file{schoolfile} could not be managed : {erreur} </translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="250"/>
        <source>La cle {id}&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation>The drive {id}&lt;br&gt;is not identified, give the owner's name</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="50"/>
        <source>{t} secondes</source>
        <translation>{t} seconds</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>pas d&apos;action pour l&apos;attribut {a}</source>
        <translation>No action for attribute {a}</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="637"/>
        <source>La dernière commande était&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="201"/>
        <source>Choissez un fichier (ou plus)</source>
        <translation>Please choose one or more files</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="205"/>
        <source>Choissez un r&#xc3;&#xa9;pertoire</source>
        <translation type="unfinished">Please choose a directory</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="220"/>
        <source>Aucune cl&#xc3;&#xa9; mod&#xc3;&#xa8;le s&#xc3;&#xa9;lectionn&#xc3;&#xa9;e</source>
        <translation type="unfinished">No template stick selected</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../mainWindow.py" line="671"/>
        <source>Démontage des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="671"/>
        <source>Êtes-vous sûr de vouloir démonter tous les baladeurs cochés de la liste ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Cocher ou décocher cette case en cliquant.</source>
        <translation type="obsolete">Chex or uncheck this box by clicking.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="774"/>
        <source>Propri&#xc3;&#xa9;taire de la cl&#xc3;&#xa9; USB ou du baladeur&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</source>
        <translation type="unfinished">Check or uncheck this box bi a click&lt;br&gt;&lt;b&gt;Double click&lt;/b&gt; to modifiy several media.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="776"/>
        <source>Point de montage de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="778"/>
        <source>Capacité de la clé USB ou du baladeur en kO ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occupée.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="780"/>
        <source>Fabricant de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="782"/>
        <source>Modèle de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="784"/>
        <source>Numéro de série de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="0"/>
        <source>Version numéro %1.%2</source>
        <translation type="obsolete">Version #%1.%2</translation>
    </message>
    <message>
        <location filename="../help.py" line="40"/>
        <source>Version num&#xc3;&#xa9;ro {major}.{minor}</source>
        <translation type="unfinished">Version number {major}.{minor}</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="772"/>
        <source>Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Ui_diskFull.py" line="54"/>
        <source>Disk size</source>
        <translation>Disk size</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="182"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="56"/>
        <source>total size</source>
        <translation>total size</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="57"/>
        <source>Used</source>
        <translation>Used</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="154"/>
        <source>ScolaSync</source>
        <translation>Scolasync</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="155"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="35"/>
        <source>Copier depuis les cles</source>
        <translation type="obsolete">Copy from the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="5"/>
        <source>Copier vers les cles</source>
        <translation type="obsolete">Copy to the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="7274514"/>
        <source>Supprimer dans les clés</source>
        <translation type="obsolete">Delete in the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="0"/>
        <source>Demonter les cles</source>
        <translation type="obsolete">Unmount the pluggable media</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="0"/>
        <source>Preferences</source>
        <translation type="obsolete">Preferences</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="181"/>
        <source>Aide</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="175"/>
        <source>Force à recompter les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="173"/>
        <source>Affiche le nombre de baladeurs connectés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="136"/>
        <source>Copier depuis les baladeurs</source>
        <translation type="obsolete">Copy from medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="139"/>
        <source>Copier vers les baladeurs</source>
        <translation type="obsolete">Copy to medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="142"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs</source>
        <translation type="obsolete">Delete files or directories in medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="145"/>
        <source>Demonter les baladeurs</source>
        <translation type="obsolete">Unmount medias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="178"/>
        <source>Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="168"/>
        <source>Refaire à nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="169"/>
        <source>Refaire à nouveau la dernière opération réussie, avec les baladeurs connectés plus récemment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="166"/>
        <source>Éjecter les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="157"/>
        <source>Copier depuis les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="160"/>
        <source>Copier vers les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="163"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="301"/>
        <source>Arrêter les opérations en cours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="302"/>
        <source>Essaie d'arrêter les opérations en cours. À faire seulement si celles-ci durent trop longtemps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="100"/>
        <source>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</source>
        <translation>&lt;br /&gt;Names are availble to rename next drives you will plug in</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="101"/>
        <source>&lt;br /&gt;Cliquez sur ce bouton pour pr&#xc3;&#xa9;parer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</source>
        <translation type="unfinished">&lt;br /&gt;Click on this button to prepare a list of names to rename drives you will plug in later</translation>
    </message>
</context>
<context>
    <name>checkBoxDialog</name>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="53"/>
        <source>Gestion des cases à cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="55"/>
        <source>Cocher tous les baladeurs</source>
        <translation>Check all the medias</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="56"/>
        <source>Tout cocher</source>
        <translation>Check all</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="58"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs cochés seront décochés, et inversement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="59"/>
        <source>Inverser le choix</source>
        <translation>Toggle the checks</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="61"/>
        <source>Désélectionner les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="62"/>
        <source>Ne rien cocher</source>
        <translation>Check none</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="64"/>
        <source>Ne rien faire</source>
        <translation>Do nothing</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="65"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>diskFull</name>
    <message>
        <location filename="../Ui_mainWindow.py" line="7274400"/>
        <source>Place totale : %1 kilo-octets</source>
        <translation type="obsolete">Total size: %1 kBytes</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="7274400"/>
        <source>Place utilisée : %1 kilo-octets</source>
        <translation type="obsolete">Used size: %1 kBytes</translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="49"/>
        <source>Place totale&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Total size: {size} kilobytes</translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="50"/>
        <source>Place utilis&#xc3;&#xa9;e&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Used: {size} kilobytes</translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="427"/>
        <source>point de montage</source>
        <translation>Mount point</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="428"/>
        <source>taille</source>
        <translation>Size</translation>
    </message>
    <message>
        <location filename="../usbDisk.py" line="1"/>
        <source>vendeur</source>
        <translation type="obsolete">Vendor</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="430"/>
        <source>modèle de disque</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="431"/>
        <source>numéro de série</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="434"/>
        <source>cocher</source>
        <translation>Check</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="201"/>
        <source>owner</source>
        <translation>Owner</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="429"/>
        <source>marque</source>
        <translation>vendor</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="286"/>
        <source>Partition ajoutée %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="301"/>
        <source>Échec au montage du disque : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="332"/>
        <source>Disque ajouté : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="362"/>
        <source>Changement pour le disque %s</source>
        <translation>Change for the disk %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="371"/>
        <source>Disque débranché du système : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="343"/>
        <source>On n&apos;ajoute pas le disque : partition non-USB</source>
        <translation>Not adding a disk: no USB partition</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="308"/>
        <source>On n&apos;ajoute pas le disque : partition vide</source>
        <translation>No disk added: empty partition</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="330"/>
        <source>Disque déjà ajouté auparavant : %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
