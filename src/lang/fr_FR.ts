<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Aide</name>
    <message>
        <location filename="../Ui_help.py" line="152"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="151"/>
        <source>Mode d&apos;emploi</source>
        <translation>Mode d'emploi</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="154"/>
        <source>ScolaSync, pour gerer les fichiers des baladeurs</source>
        <translation>ScolaSync, pour gérer les fichiers des baladeurs</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="155"/>
        <source>Numero de version :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="156"/>
        <source>Auteurs</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="157"/>
        <source>Licence</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="158"/>
        <source>Langues et traductions</source>
        <translation>Langues et traductions</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="159"/>
        <source>A propos</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="160"/>
        <source>Fermer</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="153"/>
        <source>Manuel</source>
        <translation>Manuel</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="171"/>
        <source>Choix des fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="172"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="173"/>
        <source>Ordinateur</source>
        <translation>Ordinateur</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="175"/>
        <source>Disque dur</source>
        <translation>Disque dur</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="195"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="181"/>
        <source>Baladeur</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="184"/>
        <source>Disque flash</source>
        <translation>Disque flash</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="187"/>
        <source>Cle USB</source>
        <translation>Clé USB</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="191"/>
        <source>Ajouter a la liste</source>
        <translation>Ajouter à la liste</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="194"/>
        <source>Retirer de la liste</source>
        <translation>Retirer de la liste</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="196"/>
        <source>Abandonner</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="197"/>
        <source>Continuer ...</source>
        <translation>Continuer ...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>Double-clic non pris en compte</source>
        <translation>Double-clic non pris en compte</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Veuillez choisir au moins un fichier</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="563"/>
        <source>Choix de la destination ...</source>
        <translation type="unfinished">Choix de fichiers à copier</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voir les copies</source>
        <translation>Voir les copies</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Destination manquante</source>
        <translation>Destination manquante</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Veuillez choisir une destination pour la copie des fichiers</source>
        <translation>Veuillez choisir une destination pour la copie des fichiers</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="140"/>
        <source>Zone de recherche</source>
        <translation>Zone de recherche</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="162"/>
        <source>Rechercher (fichier) ...</source>
        <translation>Rechercher (fichier) ...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="92"/>
        <source>Preferences de ScolaSync</source>
        <translation>Preferences de ScolaSync</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="93"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="99"/>
        <source>Travail</source>
        <translation>Travail</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="213"/>
        <source>inconnu</source>
        <translation>inconnu</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="212"/>
        <source>La cle %1&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation type="obsolete">La clé %1&lt;br&gt;n'est pas identifiée, donnez le nom du propriétaire</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="251"/>
        <source>Entrer un nom</source>
        <translation>Entrer un nom</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="511"/>
        <source>Vous allez effacer plusieurs baladeurs</source>
        <translation>Vous allez effacer plusieurs baladeurs</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="503"/>
        <source>Choix de fichiers à supprimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="141"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Répertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="147"/>
        <source>Le répertoire des clés&lt;br&gt;où se trouvent les&lt;br&gt;documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="149"/>
        <source>Choix de clé modèle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="151"/>
        <source>Choisir une des clés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="152"/>
        <source>Liste de fichiers à traiter (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="154"/>
        <source>Supprimer l'item sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="165"/>
        <source>Rechercher (répertoire) ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="98"/>
        <source>Répertoire pour les travaux :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="177"/>
        <source>Répertoire de destination des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="179"/>
        <source>Répertoire de destination sur les baladeurs,&lt;br&gt;peut être modifié dans les préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="189"/>
        <source>Taille totale :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="504"/>
        <source>Choix de fichiers à supprimer (jokers autorisés)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Aucun fichier sélectionné</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="562"/>
        <source>Choix de fichiers à copier depuis les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="575"/>
        <source>Choisir un répertoire de destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voulez-vous voir les fichiers copiés ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="100"/>
        <source>Fichier spécial pour le manuel :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="101"/>
        <source>/usr/share/scolasync/help/manualPage_fr_FR.html</source>
        <translation>/usr/share/scolasync/help/manualPage_fr_FR.html</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="160"/>
        <source>Ligne d'édition du nom d'un fichier ou d'un répertoire.&lt;br&gt;Jokers autorisés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="157"/>
        <source>Ajouter un fichier ou un filtre défini dans la ligne d'édition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="637"/>
        <source>Réitérer la dernière commande</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="96"/>
        <source>Efface les fichiers et les répertoires après copie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="94"/>
        <source>Si la case est cochée,&lt;br&gt;les données sont transférées sur le disque dur&lt;br&gt;puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="95"/>
        <source>Si la case est cochée, les données sont transférées sur le disque dur puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="114"/>
        <source>Choix d'un groupe d'élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="72"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="118"/>
        <source>Replier toutes les classes</source>
        <translation>Replier toutes les classes</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="119"/>
        <source>Tout replier</source>
        <translation>Tout replier</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="120"/>
        <source>Cocher tous les élèves visibles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="121"/>
        <source>Cocher</source>
        <translation>Cocher</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="122"/>
        <source>Décocher tous les élèves, visibles ou non</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="123"/>
        <source>Décocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="125"/>
        <source>Ajout à la liste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="127"/>
        <source>Suppr. de la liste</source>
        <translation>Suppr. de la liste</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="132"/>
        <source>Valider</source>
        <translation>Valider</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="129"/>
        <source>N° à partir de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="128"/>
        <source>Quand la case est cochée, un numéro sera ajouté comme préfixe aux noms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="130"/>
        <source>La numérotation commence à cette valeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="124"/>
        <source>Ajouter les noms cochés de l'arbre dans la liste à droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="126"/>
        <source>Supprimer de la liste les noms en surbrillance</source>
        <translation>Supprimer de la liste les noms en surbrillance</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="131"/>
        <source>Accepter la liste actuelle et fermer ce dialogue</source>
        <translation>Accepter la liste actuelle et fermer ce dialogue</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="133"/>
        <source>Supprimer tous les noms de la liste et fermer ce dialogue</source>
        <translation>Supprimer tous les noms de la liste et fermer ce dialogue</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="134"/>
        <source>Remettre à zéro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="69"/>
        <source>Choix du propriétaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Nouveau nom du propriétaire du baladeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="115"/>
        <source>Liste d'élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="116"/>
        <source>Ouvrir un fichier ...</source>
        <translation>Ouvrir un fichier ...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="97"/>
        <source>Fichier des élèves :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="70"/>
        <source>Nommer le baladeur nouvellement connecté</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="71"/>
        <source>Changez le choix parmi cette liste si une autre ligne convient mieux</source>
        <translation>Changez le choix parmi cette liste si une autre ligne convient mieux</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="73"/>
        <source>nom actuel du baladeur</source>
        <translation>nom actuel du baladeur</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="74"/>
        <source>C'est le nom connu par votre ordinateur, s'il a déjà été défini dans le passé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="75"/>
        <source>nom proposé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="76"/>
        <source>Le nouveau nom proposé peut venir de la liste à gauche ou être modifié à la main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="77"/>
        <source>Renommer le baladeur</source>
        <translation>Renommer le baladeur</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="78"/>
        <source>Choisir comme nouveau nom</source>
        <translation>Choisir comme nouveau nom</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="79"/>
        <source>Fermer le dialogue sans rien faire</source>
        <translation>Fermer le dialogue sans rien faire</translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="80"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../choixEleves.py" line="93"/>
        <source>Le fichier {schoolfile} n&apos;a pas pu &#xc3;&#xaa;tre trait&#xc3;&#xa9; : {erreur}</source>
        <translation type="unfinished">Le fichier {schoolfile} n'a pas pu être traité : {erreur}</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="250"/>
        <source>La cle {id}&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation>La cle {id}&lt;br&gt;n'est pas identifiee, donnez le nom du proprietaire</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>pas d&apos;action pour l&apos;attribut {a}</source>
        <translation>pas d'action pour l'attribut {a}</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="637"/>
        <source>La dernière commande était&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="162"/>
        <source>%s kilo-octets</source>
        <translation>%s kilo-octets</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="165"/>
        <source>%s m&#xc3;&#xa9;ga-octets</source>
        <translation type="unfinished">%s méga-octets</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="168"/>
        <source>%s giga-octets</source>
        <translation>%s giga-octets</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="201"/>
        <source>Choissez un fichier (ou plus)</source>
        <translation>Choissez un fichier (ou plus)</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="205"/>
        <source>Choissez un r&#xc3;&#xa9;pertoire</source>
        <translation type="unfinished">Choissez un répertoire</translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="220"/>
        <source>Aucune cl&#xc3;&#xa9; mod&#xc3;&#xa8;le s&#xc3;&#xa9;lectionn&#xc3;&#xa9;e</source>
        <translation type="unfinished">Aucune clé modèle sélectionnée</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="50"/>
        <source>{t} secondes</source>
        <translation>{t} secondes</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="148"/>
        <source>Le répertoire des clés où se trouvent les documents de travail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="561"/>
        <source>Choix de fichiers à copier</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../mainWindow.py" line="671"/>
        <source>Démontage des baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="671"/>
        <source>Êtes-vous sûr de vouloir démonter tous les baladeurs cochés de la liste ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="776"/>
        <source>Point de montage de la cl&#xc3;&#xa9; USB ou du baladeur&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</source>
        <translation type="unfinished">Propriétaire de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="778"/>
        <source>Capacité de la clé USB ou du baladeur en kO ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occupée.</source>
        <translation type="unfinished">Capacité de la clé USB ou du baladeur en kO ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occupée.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="782"/>
        <source>Mod&#xc3;&#xa8;le de la cl&#xc3;&#xa9; USB ou du baladeur.</source>
        <translation type="unfinished">Fabricant de la clé USB ou du baladeur.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="784"/>
        <source>Numéro de série de la clé USB ou du baladeur.</source>
        <translation type="unfinished">Numéro de série de la clé USB ou du baladeur.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="774"/>
        <source>Propri&#xc3;&#xa9;taire de la cl&#xc3;&#xa9; USB ou du baladeur&#xc2;&#xa0;;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</source>
        <translation type="unfinished">Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</translation>
    </message>
    <message>
        <location filename="../help.py" line="40"/>
        <source>Version num&#xc3;&#xa9;ro {major}.{minor}</source>
        <translation type="unfinished">Version numéro {major}.{minor}</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="772"/>
        <source>Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="780"/>
        <source>Fabricant de la clé USB ou du baladeur.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Ui_diskFull.py" line="54"/>
        <source>Disk size</source>
        <translation>Taille du disque</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="182"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="56"/>
        <source>total size</source>
        <translation>taille totale</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="57"/>
        <source>Used</source>
        <translation>Utilisé(s)</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="154"/>
        <source>ScolaSync</source>
        <translation>ScolaSync</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="155"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="42"/>
        <source>Copier depuis les cles</source>
        <translation type="obsolete">Copier depuis les clés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="54"/>
        <source>Copier vers les cles</source>
        <translation type="obsolete">Copier vers les clés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="78"/>
        <source>Demonter les cles</source>
        <translation type="obsolete">Démonter les clés</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="90"/>
        <source>Preferences</source>
        <translation type="obsolete">Préférences</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="181"/>
        <source>Aide</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="175"/>
        <source>Force à recompter les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="173"/>
        <source>Affiche le nombre de baladeurs connectés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="178"/>
        <source>Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="168"/>
        <source>Refaire à nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="169"/>
        <source>Refaire à nouveau la dernière opération réussie, avec les baladeurs connectés plus récemment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="166"/>
        <source>Éjecter les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="157"/>
        <source>Copier depuis les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="160"/>
        <source>Copier vers les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="163"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="301"/>
        <source>Arrêter les opérations en cours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="302"/>
        <source>Essaie d'arrêter les opérations en cours. À faire seulement si celles-ci durent trop longtemps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="100"/>
        <source>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</source>
        <translation>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="101"/>
        <source>&lt;br /&gt;Cliquez sur ce bouton pour pr&#xc3;&#xa9;parer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</source>
        <translation type="unfinished">&lt;br /&gt;Cliquez sur ce bouton pour préparer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</translation>
    </message>
</context>
<context>
    <name>checkBoxDialog</name>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="53"/>
        <source>Gestion des cases à cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="55"/>
        <source>Cocher tous les baladeurs</source>
        <translation>Cocher tous les baladeurs</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="56"/>
        <source>Tout cocher</source>
        <translation>Tout cocher</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="58"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs cochés seront décochés, et inversement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="59"/>
        <source>Inverser le choix</source>
        <translation>Inverser le choix</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="61"/>
        <source>Désélectionner les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="62"/>
        <source>Ne rien cocher</source>
        <translation>Ne rien cocher</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="64"/>
        <source>Ne rien faire</source>
        <translation>Ne rien faire</translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="65"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>diskFull</name>
    <message>
        <location filename="../diskFull.py" line="49"/>
        <source>Place totale&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Place totale : {size} kilo-octets</translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="50"/>
        <source>Place utilis&#xc3;&#xa9;e&#xc2;&#xa0;: {size} kilo-octets</source>
        <translation type="unfinished">Place utilisée : {size} kilo-octets</translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="427"/>
        <source>point de montage</source>
        <translation>point de montage</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="428"/>
        <source>taille</source>
        <translation>taille</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="434"/>
        <source>cocher</source>
        <translation>cocher</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="201"/>
        <source>owner</source>
        <translation>propriétaire</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="430"/>
        <source>modèle de disque</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="431"/>
        <source>numéro de série</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="429"/>
        <source>marque</source>
        <translation>marque</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="286"/>
        <source>Partition ajoutée %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="301"/>
        <source>Échec au montage du disque : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="332"/>
        <source>Disque ajouté : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="362"/>
        <source>Changement pour le disque %s</source>
        <translation>Changement pour le disque %s</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="371"/>
        <source>Disque débranché du système : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="343"/>
        <source>On n&apos;ajoute pas le disque : partition non-USB</source>
        <translation>On n'ajoute pas le disque : partition non-USB</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="308"/>
        <source>On n&apos;ajoute pas le disque : partition vide</source>
        <translation>On n'ajoute pas le disque : partition vide</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="330"/>
        <source>Disque déjà ajouté auparavant : %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
