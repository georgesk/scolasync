<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Aide</name>
    <message>
        <location filename="../Ui_help.py" line="152"/>
        <source>Aide</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="151"/>
        <source>Mode d&apos;emploi</source>
        <translation>Ordens para uso</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="159"/>
        <source>A propos</source>
        <translation>Sobre</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="154"/>
        <source>ScolaSync, pour gerer les fichiers des baladeurs</source>
        <translation>ScolaSync, para a gestão de ficheiros dos leitores de MP3</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="155"/>
        <source>Numero de version :</source>
        <translation>Número de versão :</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="156"/>
        <source>Auteurs</source>
        <translation>Autores</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="157"/>
        <source>Licence</source>
        <translation>Licença</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="158"/>
        <source>Langues et traductions</source>
        <translation>Idiomas e traduções</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="160"/>
        <source>Fermer</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../Ui_help.py" line="153"/>
        <source>Manuel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../mainWindow.py" line="503"/>
        <source>Choix de fichiers à supprimer</source>
        <translation>Escolha dos ficheiros a suprimir</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="140"/>
        <source>Zone de recherche</source>
        <translation>Zona de pesquisa</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="141"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Répertoire des &lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documents de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;(new line)
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;(new line)
p, li { white-space: pre-wrap; }(new line)
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;(new line)
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Pasta dos &lt;/p&gt;(new line)
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;documentos de &lt;br /&gt;travail&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="147"/>
        <source>Le répertoire des clés&lt;br&gt;où se trouvent les&lt;br&gt;documents de travail</source>
        <translation>A pasta das chaves&lt;br&gt;aonde se encontram&lt;br&gt;os documentos de trabalho</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="148"/>
        <source>Le r&#xc3;&#xa9;pertoire des cl&#xc3;&#xa9;s o&#xc3;&#xb9; se trouvent les documents de travail</source>
        <translation type="unfinished">Escrever o nome de um ficheiro ou de uma pasta.&lt;br&gt;Jokers autorizados</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="149"/>
        <source>Choix de clé modèle</source>
        <translation>Escolha da chave modelo</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="151"/>
        <source>Choisir une des clés</source>
        <translation>Escolher uma das chaves</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="152"/>
        <source>Liste de fichiers à traiter (jokers autorisés)</source>
        <translation>Lista de ficheiros a tratar (jokers autorizados)</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="154"/>
        <source>Supprimer l'item sélectionné</source>
        <translation>Suprimir o elemento selecionado</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="195"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="150"/>
        <source>Ajouter un fichier ou un filtre</source>
        <translation type="obsolete">Acrescentar um ficheiro ou um filtro</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="162"/>
        <source>Rechercher (fichier) ...</source>
        <translation>Procurar (ficheiro) ...</translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="165"/>
        <source>Rechercher (répertoire) ...</source>
        <translation>Procurar (pasta) ...</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="92"/>
        <source>Preferences de ScolaSync</source>
        <translation>Preferências de ScolaSync</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="93"/>
        <source>Preferences</source>
        <translation>Preferências</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="100"/>
        <source>Les cases à cocher permettent de n'agir que sur une part des baladeurs</source>
        <translation type="obsolete">As casas a assinalar permitem só de agir numa parte dos leitores</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="101"/>
        <source>/usr/share/scolasync/help/manualPage_fr_FR.html</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="98"/>
        <source>Répertoire pour les travaux :</source>
        <translation>Pasta para os trabalhos :</translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="99"/>
        <source>Travail</source>
        <translation>Trabalho</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="171"/>
        <source>Choix des fichiers à copier</source>
        <translation>Escolha dos ficheiros a copiar</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="172"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="173"/>
        <source>Ordinateur</source>
        <translation>Computador</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="175"/>
        <source>Disque dur</source>
        <translation>Disco duro</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="177"/>
        <source>Répertoire de destination des baladeurs</source>
        <translation>Pasta de destinação dos leitores</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="179"/>
        <source>Répertoire de destination sur les baladeurs,&lt;br&gt;peut être modifié dans les préférences</source>
        <translation>Pasta de destinação nos leitores,&lt;br&gt;pode ser modificada nas preferênçias</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="181"/>
        <source>Baladeur</source>
        <translation>Leitor</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="184"/>
        <source>Disque flash</source>
        <translation>Disco flash</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="187"/>
        <source>Cle USB</source>
        <translation>Chave USB</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="189"/>
        <source>Taille totale :</source>
        <translation>Tamanho total :</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="191"/>
        <source>Ajouter a la liste</source>
        <translation>Acrenscentar à lista</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="194"/>
        <source>Retirer de la liste</source>
        <translation>Tirar da lista</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="196"/>
        <source>Abandonner</source>
        <translation>Abandonar</translation>
    </message>
    <message>
        <location filename="../Ui_copyToDialog1.py" line="197"/>
        <source>Continuer ...</source>
        <translation>Continuar...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>Double-clic non pris en compte</source>
        <translation>Clique-duplo não é considerado</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="280"/>
        <source>pas d&apos;action pour l&apos;attribut %1</source>
        <translation type="obsolete">sem ação para o atributo %1</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="504"/>
        <source>Choix de fichiers à supprimer (jokers autorisés)</source>
        <translation>Escolha de ficheiros a suprimir (jokers autorizados)</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Aucun fichier sélectionné</source>
        <translation>Nenhum ficheiro selecionado</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="567"/>
        <source>Veuillez choisir au moins un fichier</source>
        <translation>Escolha pelo menos um ficheiro</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="561"/>
        <source>Choix de fichiers à copier</source>
        <translation>Escolha de ficheiros a copiar</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="562"/>
        <source>Choix de fichiers à copier depuis les baladeurs</source>
        <translation>Escolha de ficheiros a copiar a partir dos leitores</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="563"/>
        <source>Choix de la destination ...</source>
        <translation>Escolha do destino ...</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="575"/>
        <source>Choisir un répertoire de destination</source>
        <translation>Escolher uma pasta de destinação</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voir les copies</source>
        <translation>Ver os devers</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="606"/>
        <source>Voulez-vous voir les fichiers copiés ?</source>
        <translation>Quer ver os ficheiros copiados ?</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Destination manquante</source>
        <translation>Falta de destinação</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="614"/>
        <source>Veuillez choisir une destination pour la copie des fichiers</source>
        <translation>Escolha um destino para a cópia dos ficheiros</translation>
    </message>
    <message>
        <location filename="." line="1768190543"/>
        <source>Liste de périphériques modifiée pour Scolasync</source>
        <translation type="obsolete">Lista de periphéricos modificada para o Scolasync</translation>
    </message>
    <message>
        <location filename="." line="1768190543"/>
        <source>Une clé USB ou un baladeur numérique au moins a change de statut. Vérifiez éventuellement la liste des périphériques de Scolasync</source>
        <translation type="obsolete">Pelo menos uma chave USB ou um leitor numérico mudou de estatuto. Verifique eventualmente a lista dos periphéricos de Scolasync</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="162"/>
        <source>%s kilo-octets</source>
        <translation type="unfinished">%s kilobytes</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="165"/>
        <source>%s méga-octets</source>
        <translation type="unfinished">%s megabytes</translation>
    </message>
    <message>
        <location filename="../copyToDialog1.py" line="168"/>
        <source>%s giga-octets</source>
        <translation type="unfinished">%s gigabytes</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="213"/>
        <source>inconnu</source>
        <translation>desconhecido</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="212"/>
        <source>La cle %1&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation type="obsolete">A chave %1&lt;br&gt;não està identificada, dê o nome do dono</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="251"/>
        <source>Entrer un nom</source>
        <translation>Escreva um nome</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="511"/>
        <source>Vous allez effacer plusieurs baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="100"/>
        <source>Fichier spécial pour le manuel :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="160"/>
        <source>Ligne d'édition du nom d'un fichier ou d'un répertoire.&lt;br&gt;Jokers autorisés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_chooseInSticks.py" line="157"/>
        <source>Ajouter un fichier ou un filtre défini dans la ligne d'édition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="637"/>
        <source>Réitérer la dernière commande</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="96"/>
        <source>Efface les fichiers et les répertoires après copie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="94"/>
        <source>Si la case est cochée,&lt;br&gt;les données sont transférées sur le disque dur&lt;br&gt;puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="95"/>
        <source>Si la case est cochée, les données sont transférées sur le disque dur puis effacée des baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="114"/>
        <source>Choix d'un groupe d'élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="72"/>
        <source>Actions</source>
        <translation type="unfinished">Ações</translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="118"/>
        <source>Replier toutes les classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="119"/>
        <source>Tout replier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="120"/>
        <source>Cocher tous les élèves visibles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="121"/>
        <source>Cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="122"/>
        <source>Décocher tous les élèves, visibles ou non</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="123"/>
        <source>Décocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="125"/>
        <source>Ajout à la liste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="127"/>
        <source>Suppr. de la liste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="132"/>
        <source>Valider</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="129"/>
        <source>N° à partir de</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="128"/>
        <source>Quand la case est cochée, un numéro sera ajouté comme préfixe aux noms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="130"/>
        <source>La numérotation commence à cette valeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="124"/>
        <source>Ajouter les noms cochés de l'arbre dans la liste à droite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="126"/>
        <source>Supprimer de la liste les noms en surbrillance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="131"/>
        <source>Accepter la liste actuelle et fermer ce dialogue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="133"/>
        <source>Supprimer tous les noms de la liste et fermer ce dialogue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="134"/>
        <source>Remettre à zéro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="69"/>
        <source>Choix du propriétaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="70"/>
        <source>Nouveau nom du propriétaire du baladeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="115"/>
        <source>Liste d'élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_choixEleves.py" line="116"/>
        <source>Ouvrir un fichier ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_preferences.py" line="97"/>
        <source>Fichier des élèves :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="70"/>
        <source>Nommer le baladeur nouvellement connecté</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="71"/>
        <source>Changez le choix parmi cette liste si une autre ligne convient mieux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="73"/>
        <source>nom actuel du baladeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="74"/>
        <source>C'est le nom connu par votre ordinateur, s'il a déjà été défini dans le passé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="75"/>
        <source>nom proposé</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="76"/>
        <source>Le nouveau nom proposé peut venir de la liste à gauche ou être modifié à la main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="77"/>
        <source>Renommer le baladeur</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="78"/>
        <source>Choisir comme nouveau nom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="79"/>
        <source>Fermer le dialogue sans rien faire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_nameAdrive.py" line="80"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../choixEleves.py" line="93"/>
        <source>Échec à l'ouverture du fichier élèves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="250"/>
        <source>La cle {id}&lt;br&gt;n&apos;est pas identifiee, donnez le nom du proprietaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../choixEleves.py" line="93"/>
        <source>Le fichier {schoolfile} n'a pas pu être traité : {erreur}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="367"/>
        <source>pas d&apos;action pour l&apos;attribut {a}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="637"/>
        <source>La dernière commande était&lt;br&gt;{cmd}&lt;br&gt;Voulez-vous la relancer avec les nouveaux baladeurs ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="201"/>
        <source>Choissez un fichier (ou plus)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="205"/>
        <source>Choissez un répertoire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../chooseInSticks.py" line="220"/>
        <source>Aucune clé modèle sélectionnée</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.py" line="50"/>
        <source>{t} secondes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../help.py" line="41"/>
        <source>Version numéro %1.%2</source>
        <translation type="obsolete">Versão número %1.%2</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="671"/>
        <source>Démontage des baladeurs</source>
        <translation>Desmontagem dos leitores</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="671"/>
        <source>Êtes-vous sûr de vouloir démonter tous les baladeurs cochés de la liste ?</source>
        <translation>Tem a certeza de querer desmontar todos os leitores assinalados ?</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="697"/>
        <source>Cocher ou décocher cette case en cliquant.</source>
        <translation type="obsolete">Assinalar ou desmarcar esta casa clicando.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="774"/>
        <source>Propriétaire de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour modifier.</source>
        <translation>Dono da chave ou do leitor ;&lt;br&gt;&lt;b&gt;Clique-duplo&lt;/b&gt; para modificar.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="776"/>
        <source>Point de montage de la clé USB ou du baladeur ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir les fichiers.</source>
        <translation>Ponto de montagem da chave ou do leitor ; &lt;br&gt;&lt;b&gt;Clique-duplo&lt;/b&gt; para ver os ficheiros.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="778"/>
        <source>Capacité de la clé USB ou du baladeur en kO ;&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour voir la place occupée.</source>
        <translation>Capacidade da chave USB ou do leitor en kB ;&lt;br&gt;&lt;b&gt;Clique-duplo&lt;/b&gt; para ver o espaço ocupado.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="780"/>
        <source>Fabricant de la clé USB ou du baladeur.</source>
        <translation>Fabricante da chave USB ou do leitor.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="782"/>
        <source>Modèle de la clé USB ou du baladeur.</source>
        <translation>Modelo da chave ou do leitor.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="784"/>
        <source>Numéro de série de la clé USB ou du baladeur.</source>
        <translation>Número de série da chave ou do leitor.</translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="772"/>
        <source>Cocher ou décocher cette case en cliquant.&lt;br&gt;&lt;b&gt;Double-clic&lt;/b&gt; pour agir sur plusieurs baladeurs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../help.py" line="40"/>
        <source>Version numéro {major}.{minor}</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Ui_mainWindow.py" line="154"/>
        <source>ScolaSync</source>
        <translation>Scolasync</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="155"/>
        <source>Actions</source>
        <translation>Ações</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="42"/>
        <source>Copier depuis les cles</source>
        <translation type="obsolete">Copiar a partir dos leitores</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="182"/>
        <source>...</source>
        <translation>... </translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="54"/>
        <source>Copier vers les cles</source>
        <translation type="obsolete">Copiar para os leitores</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="66"/>
        <source>Supprimer dans les clés</source>
        <translation type="obsolete">Suprimir nos leitores</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="78"/>
        <source>Demonter les cles</source>
        <translation type="obsolete">Desmontar os leitores</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="90"/>
        <source>Preferences</source>
        <translation type="obsolete">Preferéncias</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="181"/>
        <source>Aide</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="54"/>
        <source>Disk size</source>
        <translation>Tamanho do disco</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="56"/>
        <source>total size</source>
        <translation>Tamanho total</translation>
    </message>
    <message>
        <location filename="../Ui_diskFull.py" line="57"/>
        <source>Used</source>
        <translation>Utilizado</translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="175"/>
        <source>Force à recompter les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="173"/>
        <source>Affiche le nombre de baladeurs connectés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="178"/>
        <source>Préférences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="168"/>
        <source>Refaire à nouveau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="169"/>
        <source>Refaire à nouveau la dernière opération réussie, avec les baladeurs connectés plus récemment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="166"/>
        <source>Éjecter les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="157"/>
        <source>Copier depuis les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="160"/>
        <source>Copier vers les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_mainWindow.py" line="163"/>
        <source>Effacer des fichiers ou des dossiers dans les baladeurs sélectionnés</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="301"/>
        <source>Arrêter les opérations en cours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="302"/>
        <source>Essaie d'arrêter les opérations en cours. À faire seulement si celles-ci durent trop longtemps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="100"/>
        <source>&lt;br /&gt;Des noms sont disponibles pour renommer les prochains baladeurs que vous brancherez</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainWindow.py" line="101"/>
        <source>&lt;br /&gt;Cliquez sur ce bouton pour préparer une liste de noms afin de renommer les prochains baladeurs que vous brancherez</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>checkBoxDialog</name>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="53"/>
        <source>Gestion des cases à cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="55"/>
        <source>Cocher tous les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="56"/>
        <source>Tout cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="58"/>
        <source>Inverser le choix de baladeurs&lt;br&gt;Les baladeurs cochés seront décochés, et inversement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="59"/>
        <source>Inverser le choix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="61"/>
        <source>Désélectionner les baladeurs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="62"/>
        <source>Ne rien cocher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="64"/>
        <source>Ne rien faire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Ui_checkBoxDialog.py" line="65"/>
        <source>Échappement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>diskFull</name>
    <message>
        <location filename="../diskFull.py" line="49"/>
        <source>Place totale : {size} kilo-octets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../diskFull.py" line="50"/>
        <source>Place utilisée : {size} kilo-octets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>uDisk</name>
    <message>
        <location filename="../usbDisk2.py" line="427"/>
        <source>point de montage</source>
        <translation>Ponto de montagem</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="428"/>
        <source>taille</source>
        <translation>tamanho</translation>
    </message>
    <message>
        <location filename="../usbDisk.py" line="65"/>
        <source>vendeur</source>
        <translation type="obsolete">vendedor</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="430"/>
        <source>modèle de disque</source>
        <translation>modelo de disco</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="431"/>
        <source>numéro de série</source>
        <translation>número de série</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="434"/>
        <source>cocher</source>
        <translation>assinalar</translation>
    </message>
    <message>
        <location filename="../ownedUsbDisk.py" line="201"/>
        <source>owner</source>
        <translation>dono</translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="429"/>
        <source>marque</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="286"/>
        <source>Partition ajoutée %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="301"/>
        <source>Échec au montage du disque : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="332"/>
        <source>Disque ajouté : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="362"/>
        <source>Changement pour le disque %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="371"/>
        <source>Disque débranché du système : %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="343"/>
        <source>On n&apos;ajoute pas le disque : partition non-USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="308"/>
        <source>On n&apos;ajoute pas le disque : partition vide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../usbDisk2.py" line="330"/>
        <source>Disque déjà ajouté auparavant : %s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
