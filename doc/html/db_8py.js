var db_8py =
[
    [ "checkVersion", "db_8py.html#ada90bce10163107c35dd61955edf9307", null ],
    [ "hasStudent", "db_8py.html#a4cce1a7772d666bb9c52a2d4ba22c9cd", null ],
    [ "knowsId", "db_8py.html#a708cc3325644f79a8da6817cf131e3e8", null ],
    [ "openDb", "db_8py.html#a0df14ce45b703f8486dc9205ac9246ad", null ],
    [ "readPrefs", "db_8py.html#a1a44074f833d8283643343b09cff02b6", null ],
    [ "readStudent", "db_8py.html#a9afc0ee6dce3ce82783bb0aaa349532f", null ],
    [ "setWd", "db_8py.html#ada6f363fccebc105cf7ca85bdb5eccdc", null ],
    [ "tattooList", "db_8py.html#a2734729596ad4aa825ed66b97ddd7209", null ],
    [ "writePrefs", "db_8py.html#a6628948dc0e29baf0d368288dbb676be", null ],
    [ "writeStudent", "db_8py.html#ab88098a0a4df23901dffda6197968088", null ],
    [ "cursor", "db_8py.html#a23dd9ed4abbc2dcd952158347944fe39", null ],
    [ "database", "db_8py.html#a11a7f300f3f10c0ab24cf7b95cc4e5b2", null ],
    [ "licence", "db_8py.html#aa073cf63c80489da19924aaa911726f3", null ]
];